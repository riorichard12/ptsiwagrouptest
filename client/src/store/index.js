import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
const url = 'http://localhost:5050/movies'
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    movies:[],
    movieById:{},
    isLoading : false
  },
  getters: {
  },
  mutations: {
    FETCH_MOVIES(state,payload){
      state.movies = payload
    },
    FETCH_MOVIE(state,payload){
      state.movieById = payload
    },
    CHANGE_ISLOADING(state,payload){
      state.isLoading = payload
    },
    ADD_MOVIE(state,payload){
      let newMovies = state.movies
      newMovies.push(payload)
      state.movies = newMovies
    }
  },
  actions: {
    fetchMovies(state,query){
      if(query){
        return axios.get(url+'?title='+query)
      }
      return axios.get(url)
    },
    fetchMovieById(_,id){
      return axios.get(url+`/${id}`)
    },
    addMovie(_,data){
      return axios.post(url,data)
    },
    updateMovie(_,data){
      return axios.put(url,data)
    },
    deleteMovieById(_,id){
      return axios.delete(url+`/${id}`)
    }
  },
  modules: {
  }
})
